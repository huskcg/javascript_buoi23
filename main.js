//ex1
var salary = document.getElementById("salary");
var days = document.getElementById("days");
//ex2
var number1 = document.getElementById("number1");
var number2 = document.getElementById("number2");
var number3 = document.getElementById("number3");
var number4 = document.getElementById("number4");
var number5 = document.getElementById("number5");
//ex3
var exchangeRate = 23500;
var money = document.getElementById("money");
//ex4
var length = document.getElementById("length");
var width = document.getElementById("width");
//ex5
var number = document.getElementById("number");

function calcExcercise1() {
  var result = salary.value * 1 * days.value * 1;
  document.getElementById(
    "result-ex1"
  ).innerHTML = `<span>Result:</span> ${result}`;
}
function calcExcercise2() {
  var result =
    (number1.value * 1 +
      number2.value * 1 +
      number3.value * 1 +
      number4.value * 1 +
      number5.value * 1) /
    5;
  document.getElementById(
    "result-ex2"
  ).innerHTML = `<span>Result:</span> ${result}`;
}
function calcExcercise3() {
  var result = money.value * 1 * exchangeRate;
  document.getElementById(
    "result-ex3"
  ).innerHTML = `<span>Result:</span> ${Intl.NumberFormat("vn-VN").format(
    result
  )}`;
}
function calcExcercise4() {
  var area = length.value * 1 * width.value * 1;
  var perimeter = 2 * (length.value * 1 + width.value * 1);
  document.getElementById(
    "result-ex4"
  ).innerHTML = `<span>Diện tích:</span> ${area}; <span>Chu vi:</span> ${perimeter}`;
}
function calcExcercise5() {
  var unit = (number.value * 1) % 10;
  var ten = Math.floor((number.value * 1) / 10);
  var result = unit + ten;
  document.getElementById(
    "result-ex5"
  ).innerHTML = `<span>Result:</span> ${result}`;
}
